Source: libgeo-coordinates-osgb-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: gregor herrmann <gregoa@debian.org>,
           Dominic Hargreaves <dom@earth.li>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13)
Build-Depends-Indep: libfile-sharedir-install-perl,
                     libfile-share-perl,
                     perl
Standards-Version: 4.1.5
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libgeo-coordinates-osgb-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libgeo-coordinates-osgb-perl.git
Homepage: https://metacpan.org/release/Geo-Coordinates-OSGB

Package: libgeo-coordinates-osgb-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
		 libfile-share-perl
Multi-Arch: foreign
Description: converting module between Lat/Lon and the British National Grid
 The included modules provide an implementation of co-ordinate conversion for
 England, Wales, and Scotland based on formulae and data published by the
 Ordnance Survey of Great Britain.
 .
 The Geo::Coordinates::OSGB module provides routines to convert between
 latitude/longitude coordinates in the WGS84 or OSGB36 models and the British
 National Grid. The Geo::Coordinates::OSGB::Grid module includes some useful
 extra routines to parse and format grid references in a variety of popular
 forms (including which Landranger map your point appears on). The
 Geo::Coordinates::OSGB::Maps module provides data for British maps.
 .
 Note that due to the nature of this type of conversion, the conversions are
 only really useful in the vicinity of the British Isles. If you are elsewhere
 on the planet you need an implementation optimized for your neighbourhood.
