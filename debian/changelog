libgeo-coordinates-osgb-perl (2.20-2) unstable; urgency=medium

  [ Laurent Baillet ]
  * fix lintian wrong-path-for-interpreter error

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 11 to 13.
  * Set debhelper-compat version in Build-Depends.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata (already present in machine-readable debian/copyright).

 -- Jelmer Vernooĳ <jelmer@debian.org>  Tue, 06 Dec 2022 17:14:57 +0000

libgeo-coordinates-osgb-perl (2.20-1.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Thu, 07 Jan 2021 15:28:13 +0100

libgeo-coordinates-osgb-perl (2.20-1) unstable; urgency=medium

  * Team upload.

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Lucas Kanashiro ]
  * Import upstream version 2.20
  * Bump debhelper compatibility level to 11
  * Declare compliance with Debian Policy 4.1.5
  * Update years of upstream copyright
  * Update build and runtime dependencies

 -- Lucas Kanashiro <kanashiro@debian.org>  Mon, 23 Jul 2018 03:44:46 -0300

libgeo-coordinates-osgb-perl (2.16-1) unstable; urgency=medium

  * Team upload.
  * Add debian/upstream/metadata
  * Import upstream version 2.16
  * d/copyright: set up Toby Thurston's email

 -- Lucas Kanashiro <kanashiro.duarte@gmail.com>  Sun, 21 Feb 2016 17:56:14 -0300

libgeo-coordinates-osgb-perl (2.15-1) unstable; urgency=medium

  * New upstream release.
  * Ship new build scripts as examples.
  * Add debian/NEWS with semantics and interface changes.

 -- gregor herrmann <gregoa@debian.org>  Sat, 20 Feb 2016 21:16:45 +0100

libgeo-coordinates-osgb-perl (2.14-1) unstable; urgency=medium

  * New upstream release.
  * Drop spelling.patch, fixed upstream.

 -- gregor herrmann <gregoa@debian.org>  Sat, 13 Feb 2016 15:59:04 +0100

libgeo-coordinates-osgb-perl (2.13-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * New upstream release.
  * Drop pod.patch, fixed upstream.
  * Declare compliance with Debian Policy 3.9.7.
  * Add a patch to fix spelling mistakes in the POD.

 -- gregor herrmann <gregoa@debian.org>  Sat, 06 Feb 2016 02:35:59 +0100

libgeo-coordinates-osgb-perl (2.11-1) unstable; urgency=medium

  * New upstream release.
  * Update years of upstream and packaging copyright.
  * Update upstream license.
  * Update long description to reflect the changes in the structure of the
    modules, following the updated upstream documentation.
  * Add a patch to fix POD issues.

 -- gregor herrmann <gregoa@debian.org>  Sat, 23 Jan 2016 00:06:45 +0100

libgeo-coordinates-osgb-perl (2.09-1) unstable; urgency=medium

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * New upstream release.
  * Examples were moved in upstream tarball. Adjust debian/rules and
    debian/libgeo-coordinates-osgb-perl.examples accordingly.
  * Update years of upstream and packaging copyright.
  * Mark package as autopkgtest-able.
  * Declare compliance with Debian Policy 3.9.6.
  * Bump debhelper compatibility level to 9.

 -- gregor herrmann <gregoa@debian.org>  Sat, 31 Oct 2015 18:26:51 +0100

libgeo-coordinates-osgb-perl (2.06-1) unstable; urgency=low

  * New upstream release.
  * Drop pod-encoding.patch, not needed anymore.

 -- gregor herrmann <gregoa@debian.org>  Mon, 07 Oct 2013 20:45:03 +0200

libgeo-coordinates-osgb-perl (2.05-1) unstable; urgency=low

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * New upstream release.
  * Update years of copyright.
  * Declare compliance with Debian Policy 3.9.4.
  * Add patch to fix POD encoding error.

 -- gregor herrmann <gregoa@debian.org>  Wed, 02 Oct 2013 23:45:12 +0200

libgeo-coordinates-osgb-perl (2.04-2) unstable; urgency=low

  * Update debian/copyright to include licence modifications received
    from the Ordnance Survey; move package to main (Closes: #694621)

 -- Dominic Hargreaves <dom@earth.li>  Sun, 16 Dec 2012 13:58:35 +0000

libgeo-coordinates-osgb-perl (2.04-1) unstable; urgency=low

  [ gregor herrmann ]
  * Initial Release. (Closes: #664558)

  [ Dominic Hargreaves ]
  * Change section to non-free/perl since it is not clear that this
    package is DFSG-free. See #664558 for discussion.

 -- Dominic Hargreaves <dom@earth.li>  Sun, 28 Oct 2012 22:32:00 +0000
